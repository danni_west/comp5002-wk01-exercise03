﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication2
{
    class Program
    {
        static void Main(string[] args)
        {
            var name = "Danni";
            Console.WriteLine("What is your name?");
            name = Console.ReadLine();

            Console.WriteLine($"Thank you {name} have a good day.");
        }
    }
}
